class ScaleLayer(object):
    def __init__(self, name, scale_factor):
        self.name = name
        self.keep_prob = scale_factor

    def get_output_expr(self, input_expr):
        return input_expr * self.keep_prob

    @classmethod
    def create_copy(cls, other):
        return cls(other.keep_prob)

    @classmethod
    def create_from_state(cls, state):
        return cls(state['name'], state['scale_factor'])

    def get_state(self):
        return {'name': self.name,
                'scale_factor': self.keep_prob}
