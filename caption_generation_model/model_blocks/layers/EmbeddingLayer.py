import theano

class EmbeddingLayer(object):
    def __init__(self, name, embedding_init):
        self.name = name
        self.embedding = theano.shared(embedding_init())

    def get_output_expr(self, input_expr):
        output = self.embedding[input_expr]
        return output

    def get_parameters(self):
        return [self.embedding]

    @classmethod
    def create_copy(cls, other):
        return cls(other.name, lambda: other.embedding.get_value())

    @classmethod
    def create_from_state(cls, state):
        return cls(state['name'], lambda:state['embedding'])

    def get_state(self):
        return {'name':self.name, 'embedding':self.embedding.get_value()}