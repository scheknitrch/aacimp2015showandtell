import theano
import theano.tensor as T
class DenseLayer(object):
    def __init__(self, name, W_init, b_init=None):
        self.name = name
        self.W = theano.shared(W_init())
        if b_init:
            self.b = theano.shared(b_init())


    def get_output_expr(self, input_expr):
        if hasattr(self, 'b'):
            return T.dot(input_expr, self.W) + self.b
        else:
            return T.dot(input_expr, self.W)


    def get_parameters(self):
        if hasattr(self, "b"):
            return [self.W, self.b]
        else:
            return [self.W]

    @classmethod
    def create_copy(cls, other):
        matrix = lambda : other.W.get_value()
        if hasattr(other, "b"):
            bias = lambda : other.b.get_value()
        else:
            bias = None
        return cls(other.name, matrix, bias)

    @classmethod
    def create_from_state(cls, state):
        matrix = lambda : state["W"]
        if state['b'] is not None:
            bias = lambda : state["b"]
        else:
            bias = None
        return cls(state["name"], matrix, bias)

    def get_state(self):
        if hasattr(self, "b"):
            bias = self.b.get_value()
        else:
            bias = None
        return { "name" : self.name, "W" : self.W.get_value(), "b" : bias}
