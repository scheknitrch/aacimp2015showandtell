import theano
import numpy as np
class NonlinearityLayer(object):
    def __init__(self, name, nonlinearity):
        self.name = name
        self.nonlinearity = nonlinearity

    def get_output_expr(self, input_expr):
        return self.nonlinearity(input_expr)

    def get_parameters(self):
        return []

    @classmethod
    def create_copy(cls, other):
        cls(other.name, other.nonlinearity)

    @classmethod
    def create_from_state(cls, state):
        return cls(state["name"], state["nonlinearity"])



    def get_state(self):
        return {"name" : self.name, "nonlinearity" : self.nonlinearity}