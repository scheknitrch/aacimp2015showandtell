import theano
from theano import tensor as T
import numpy as np
from theano.tensor.nnet import sigmoid as sigma
from theano.tensor import tanh


class LstmLayer(object):
    def __init__(self, name, W_z_init, W_i_init, W_f_init, W_o_init,
                             R_z_init, R_i_init, R_f_init, R_o_init,
                             b_z_init, b_i_init, b_f_init, b_o_init):
        self.name = name
        self.W_z = theano.shared(W_z_init())
        self.W_i = theano.shared(W_i_init())
        self.W_f = theano.shared(W_f_init())
        self.W_o = theano.shared(W_o_init())

        self.R_z = theano.shared(R_z_init())
        self.R_i = theano.shared(R_i_init())
        self.R_f = theano.shared(R_f_init())
        self.R_o = theano.shared(R_o_init())

        self.b_z = theano.shared(b_z_init())
        self.b_i = theano.shared(b_i_init())
        self.b_f = theano.shared(b_f_init())
        self.b_o = theano.shared(b_o_init())

    def get_output_expr(self, input_sequence):
        c0 = T.zeros((self.W_z.shape[0], ))
        h0 = T.zeros((self.W_z.shape[0], ))
        [_, h], _ = theano.scan(fn=self.__lstm_step,
                           sequences = input_sequence,
                           outputs_info = [c0, h0])
        return h

    def __lstm_step(self,x_t,c_t1, h_t1):
        z_t = tanh(theano.dot(x_t, self.W_z) + theano.dot(h_t1, self.R_z) + self.b_z)
        i_t = sigma(theano.dot(x_t,self.W_i)+theano.dot(h_t1,self.R_i) + self.b_i)
        f_t = sigma(theano.dot(x_t,self.W_f)+theano.dot(h_t1,self.R_f) + self.b_f)
        o_t = sigma(theano.dot(x_t,self.W_o)+theano.dot(h_t1,self.R_o) + self.b_o)
        c_t = f_t * c_t1 + i_t * z_t
        h_t = o_t * tanh(c_t)
        return [c_t,h_t]

    def get_parameters(self):
        return [self.W_z, self.W_i, self.W_f, self.W_o,
                self.R_z, self.R_i, self.R_f, self.R_o,
                self.b_z, self.b_i, self.b_f, self.b_o]

    @classmethod
    def create_copy(cls, other):
        fun = lambda a: lambda: getattr(other, a).get_value()
        return cls(other.name,
                   fun('W_z'), fun('W_i'), fun('W_f'), fun('W_o'),
                   fun('R_z'), fun('R_i'), fun('R_f'), fun('R_o'),
                   fun('b_z'), fun('b_i'), fun('b_f'), fun('b_o'))

    @classmethod
    def create_from_state(cls, state):
        return cls(state["name"],lambda:state["W_z"],lambda:state["W_i"],lambda:state["W_f"],lambda:state["W_o"],
                          lambda:state["R_z"],lambda:state["R_i"],lambda:state["R_f"],lambda:state["R_o"],
                          lambda:state["b_z"],lambda:state["b_i"],lambda:state["b_f"],lambda:state["b_o"])

    def get_state(self):
        return {"name": self.name, "W_z":self.W_z.get_value(), "W_i":self.W_i.get_value(), "W_f":self.W_f.get_value(), "W_o":self.W_o.get_value(),
                "R_z":self.R_z.get_value(), "R_i":self.R_i.get_value(), "R_f":self.R_f.get_value(), "R_o":self.R_o.get_value(),
                "b_z":self.b_z.get_value(), "b_i":self.b_i.get_value(), "b_f":self.b_f.get_value(), "b_o":self.b_o.get_value()}