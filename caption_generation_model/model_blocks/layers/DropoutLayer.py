import numpy as np

from theano import tensor as T


class DropoutLayer(object):
    def __init__(self, name, dropout_prob):
        self.name = name
        self.dropout_prob = dropout_prob
        self.keep_prob = np.float32(1. - dropout_prob)

    def get_output_expr(self, input_expr):
        rng = np.random.RandomState(seed=42)
        srng = T.shared_randomstreams.RandomStreams(rng.randint(99999))
        mask = srng.binomial(p=self.keep_prob, size=input_expr.shape, dtype='float32')
        return input_expr * mask

    def get_parameters(self):
        return []

    @classmethod
    def create_copy(cls, other):
        return cls(other.name, other.dropout_prob)

    @classmethod
    def create_from_state(cls, state):
        return cls(state['name'], state['dropout_prob'])

    def get_state(self):
        return {'name': self.name,
                'dropout_prob': self.dropout_prob}
