import theano
import theano.tensor as T

class RowStackLayer(object):
    def __init__(self, name):
        self.name = name

    def get_output_expr(self, vector, matrix):
        new_matrix = T.concatenate((vector.dimshuffle('x',0), matrix))
        return new_matrix

    def get_parameters(self):
        return []

    @classmethod
    def create_copy(cls, other):
        return cls(other.name)

    @classmethod
    def create_from_state(cls, state):
        return cls(state['name'])

    def get_state(self):
        return {'name': self.name}