import theano
import numpy as np
import theano.tensor as T


shared_zeros_like = lambda var: theano.shared(np.zeros_like(var.get_value()).astype(np.float32))


# https://github.com/lisa-lab/pylearn2/pull/136
def get_nesterov_momentum_updates(loss_expr,
                                  dense_parameters, sparse_parameters,
                                  learning_rate, momentum):
    grads = T.grad(cost=loss_expr, wrt=dense_parameters + sparse_parameters)
    dense_grads = grads[:len(dense_parameters)]
    sparse_grads = grads[-len(sparse_parameters):]
    updates = []

    for p, g in zip(sparse_parameters, sparse_grads):
        updates.append((p, p - learning_rate * g))

    for p, g in zip(dense_parameters, dense_grads):
        v = shared_zeros_like(p)
        new_v = momentum * v - learning_rate * g
        new_p = p + T.sqr(momentum) * v - (np.float32(1.) + momentum) * learning_rate * g
        #new_p = p + momentum * new_v - learning_rate * g
        updates.append((v, new_v))
        updates.append((p, new_p))
    return updates